class Pessoa:
    def __init__(self, nome:str, idade:int, altura:float, peso:float, ponto:int):
        self.nome = nome.upper()
        self.idade = idade
        self.altura = altura
        self.peso = peso
        self.ponto = ponto

    def __str__(self) -> str:
        return f"{self.nome} {str(self.idade)} {(self.altura)} {str(self.peso)} {str(self.ponto)}"
    
    def imc(self):
        quadrado = self.altura * self.altura
        indice_imc = self.peso / quadrado
        return indice_imc
