from pessoa import Pessoa
from random import randint

jogador_1 = Pessoa("Djair" , 24, 1.93, 85, 0)
jogador_2 = Pessoa("Thyago" , 17, 1.76, 69, 0)
n_gerado = randint(1, 100)
print (jogador_1, jogador_2)

jogador_1.ponto = randint (1, 100) #int(input(f"{jogador_1.nome} Escolha seu numero: "))
jogador_2.ponto = randint (1, 100) #int(input(f"{jogador_2.nome} Escolha seu numero: "))  

soma1 = abs(n_gerado - jogador_1.ponto)
soma2 = abs(n_gerado - jogador_2.ponto)
print ("\nDjair inseriu", jogador_1.ponto, "e Thyago inseriu", jogador_2.ponto)
print(f"Numero sorteado {n_gerado}")

if soma1 < soma2:
    print (f"\nParabens {jogador_1.nome} voce ganhou!")
elif soma2 < soma1:
    print (f"\nParabens {jogador_2.nome} voce ganhou!")
else:  
    print (f"Parabens {jogador_1.nome} e {jogador_2.nome} voces ganharam!")
