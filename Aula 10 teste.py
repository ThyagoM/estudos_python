# Recebe o texto
nomes = input()

# Transforma o valor de entrada em uma lista onde os elementos são separados por vírgula
lista_de_nomes = nomes.split(',')

# Ordenando os itens da lista
lista_de_nomes.sort()

# Percorre e exibe cada item da lista
for nome in lista_de_nomes:
    print("Hello, " + nome)

print ("Fim da execucao")