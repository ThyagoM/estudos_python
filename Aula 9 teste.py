"""
utilizando a definicao de funcao para uma msg e a funcao len para que dependendo da msg as linhas do print seja
de acordo com o tamanho do texto
"""

def escreva(msg):
    tam = len(msg) + 4
    print ("-" * tam)
    print (f"  {msg}") #msg formatada com f e chaves
    print ("-" * tam)

#nesse modelo a impressao ja esta definida na def escreva
escreva ("administrar")

#nesse modelo a mensagem sera criada pelo usuario e definida o tamanho das linhas de acordo com o texto
escreva (input("Teste de comprimento texto 1: "))
escreva (input("Teste de comprimento texto 2: "))

#outro metodo de impressao da def
msg = input("Digite um texto de referencia: ")
escreva (msg)