from datetime import date,timedelta

'''
Temos um codigo diario, independente do dia da semana, vamos pegar como base o dia 09 de setembro de 2023 o qual consta o codigo 12192
a cada dia, o codigo incrementa 6 unidades ao valor do proprio.

partindo dessa premissa, desenvolveremos um programa, que dada a data, ele apresenta o codigo atual.

ex: 09/09/2023   -   12192
variaveis: data_inicial, codigo_inicial, data_final.
'''

def codigos_djsystem(n_codigos):
    data_inicial = date(2023,9,9)
    data_atual = date.today()
    diferenca_diaria = abs((data_inicial - data_atual).days)
    codigo = 12192 + 6 * diferenca_diaria
    dia = timedelta(days = 1)
    for cada_dia in range(n_codigos):
        print (data_atual, "-", codigo)
        data_atual = data_atual + dia
        codigo = codigo + 6
codigos_djsystem (n_codigos = int(input("Informe quantos codigos queres: ")))

'''
valor = 12210
dias_passados = timedelta(days = valor / 6)
data_agora = date.today()
data_criada = data_agora - dias_passados
print (data_criada)
'''
