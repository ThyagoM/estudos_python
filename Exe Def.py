'''
escreva uma funcao que recebe um objeto de colecao e retorne o valor do maior numero 
dentro dessa colecao faca outra funcao que retorna o numero dessa colecao
'''

# def e definicao de funcao,
# maior tem colecao como objeto, maior_item e igual a colecao que contem uma lista, for percorre a lista item em 'in' colecao,
# se 'if' que verifica se contem informacao na lista do objeto colecao definida com a variavel maior_item contiver algo, 
# retorne o maior_item que e igual a item, que item percorre a colecao e colecao que contem objeto na lista, 
# printa ao final 'maior' que foi definido 'def' como colecao.

# todas as informacoes digitadas sao referencias do que entendi da funcao, podendo chegar a algo mais conciso futuramente com a pratica


def maior(colecao):
    maior_item = colecao[0]
    for item in colecao:
        if item > maior_item:
            maior_item = item
    return maior_item   

def menor(colecao):
    menor_item = colecao[0]
    for item in colecao:
        if item < menor_item:
            menor_item = item
    return menor_item

print (maior([1,2,3,4,5,6,7,100]))
print (menor([20,30,-10,15,87]))

