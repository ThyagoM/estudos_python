#aprendendo a def uma funcao para um argumento especifico

def divida (x, y):
    d = x / y
    print (d)


def soma (a, b):
    s = a + b
    print (s)


def linha():
    print ("-" * 20)


linha()   
#divisao fixa
divida (40, 4)
divida (9, 3)
divida (10, 2)

linha()
#somatorio
soma (50, 4)
soma (43, 17)
soma (1986, 36)

linha()

#Metros quadrados do seu terreno
def l():
    print ("-" * 60)

    
def area(largura,  comprimento):
    a = largura * comprimento
    print (f"o seu terreno de {largura} x {comprimento} tem o tamanho total de {a}m2")


print ("Calcule o tamanho em M2 de seu terreno")
l()
largura = float(input("informe a largura (m): "))
comprimento = float(input("informe o comprimento (m): "))
l()
area(largura, comprimento)
l()