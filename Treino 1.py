def lin():
    print('_' * 30)

lin()
print ('quatro métodos de string:')
lin()

len = '1 - Contador de caracteres'
print (len)

#len() - obtém o comprimento (o número de caracteres) de uma string!
    ##  parrot = "Norwegian Blue"
    ##  print len(parrot)

lower = '2 - transforma tudo em letras minuscula'
print (lower)

#lower() - método para se livrar de todas as letras maiúsculas em suas strings
    ##  parrot = "Norwegian Blue"
    ##  print parrot.lower()

upper = '3 - transforma tudo em letras maiusculas'
print (upper)

#método para se livrar de todas as letras minusculas em suas strings
    ##  parrot = "norwegian blue"
    ##  print parrot.upper()

str = '4 - transforma uma nao string em string'
print (str)

#str transforma uma nao string em string
    ##  pi = 3.14
    ##  print str(pi)

lin()
print ('Exemplo len e upper')
lin()

#exemplo de len(contar caracteres) e upper, transformalo em maiusculo
    ##  name = 'thyago marcel ramos de souza'
    ##  print len(name)
    ##  print name.upper()
lin()
print ('Exemplo datetime e concatenar com %')
lin()

#exemplo melhor que concatenar com % ou %02d e assim por diante
    ##  from datetime import datetime
    ##  now = datetime.now()
    ##  print '%02d/%02d/%04d' % (now.month, now.day, now.year)

    ##  from datetime import datetime
    ##  now = datetime.now()

    ##  print '%02d/%02d/%04d %02d:%02d:%02d' % (now.month, now.day, now.year, now.hour, now.minute, now.second)
lin()

