Igual a ( ==)

>>> 2 == 2
True
>>> 2 == 5
False

Diferente de ( !=)

>>> 2 != 5
True
>>> 2 != 2
False

menor que ( <)

>>> 2 < 5
True
>>> 5 < 2
False

Menor ou igual a ( <=)

>>> 2 <= 2
True
>>> 5 <= 2
False 

Maior que ( >)

>>> 5 > 2
True
>>> 2 > 5
False

Maior ou igual a ( >=)

>>> 5 >= 5
True
>>> 2 >= 5
False
