last_semester_gradebook = [["politics", 80], ["latin", 96], ["dance", 97], ["architecture", 65]]#listas dentro de uma lista

#APPEND
subjects = ["physics", "calculus", "poetry", "history"]#lista de objetos
grades = [98, 97, 85, 88]#lista de objetos
gradebook = [["physics",	98], ["calculus",	97], ["poetry",	85], ["history",	88]]#listas dentro de uma lista
print (gradebook)#verificando
gradebook.append (["computer science", 100])#adicionando uma nova lista dentro da lista com .append por ultimo
gradebook.append (["visual arts", 93])#adicionando uma nova lista dentro da lista com .append por ultimo
gradebook[5][1] = 98 #modificando no indice 5 o item 1 para novo valor
gradebook[2].remove (85)#removendo no indice 2 com .remove o valor
gradebook[2].append ("Pass") #adicionando no indice 2 o item 1 com o valor de pass
full_gradebook = last_semester_gradebook + gradebook #cirando nova variavel com o valor da lista 1 + lista 2, a lista 2 vai para o final da lista 1
print (full_gradebook)#verificando

#INSERT
front_display_list = ["Mango", "Filet Mignon", "Chocolate Milk"]#lista com obetos
print(front_display_list)#verificando
front_display_list.insert(0, "Pineapple")#inserindo como .insert pineapple comoo indice 0
print(front_display_list)#verificando

#POP
data_science_topics = ["Machine Learning", "SQL", "Pandas", "Algorithms", "Statistics", "Python 3"]#lista de objetos
print(data_science_topics)#verificando
data_science_topics.pop()#removendo com .pop o ultimo item da lista, como parenteses vazio
print(data_science_topics)#verificando
data_science_topics.pop(3)#remodendo com .pop o item da lista no indice 3
print(data_science_topics)#verificando

#RANGE
range_five_three = range(5, 15, 3) # variavel contendo um range comecando de 5, parando em 14 que e antes ultimo numero informado 15 e com 3 intervalos ( o intevalo fica por ultimo.)
range_diff_five = range(0, 40, 5) #comeca com zero termina em 39 e com intervalo de 5, printara 5,10,15,20,25,30,35

#LEN
long_list = [1, 5, 6, 7, -23, 69.5, True, "very", "long", "list", "that", "keeps", "going.", "Let's", "practice", "getting", "the", "length"]

big_range = range(2, 3000, 10) #variavel com um renge que comeca em 2,  termina em 2999 com intervalo de 10. exe 2,12,24...
print(len(long_list))#verificando
long_list_len = len(long_list)#variavel e igual a .len que contara o numero de itens na long_list
print(long_list_len)#verificando
big_range_length = len(big_range)#variavel e igual a .len que contara o numero de itens na big_range
print(big_range_length)#verificando
big_range = range(2, 3000, 100)#aterado big_range para ter um inetrvalo em 100.